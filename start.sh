#!/bin/bash

docker network connect bridge phabricator

docker-compose up -d db
sleep 10
docker-compose up -d phabricator
