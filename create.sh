set -x

mkdir -p /srv/docker-data/phabricator/db-data
mkdir -p /srv/docker-data/phabricator/db-conf
mkdir -p /srv/docker-data/phabricator/data
mkdir -p /srv/docker-data/phabricator/repo

docker volume create --name phabricator-db-data \
 --opt type=none \
 --opt device=/srv/docker-data/phabricator/db-data \
 --opt o=bind
docker volume create --name phabricator-db-conf \
 --opt type=none \
 --opt device=/srv/docker-data/phabricator/db-conf \
 --opt o=bind
docker volume create --name phabricator-data \
 --opt type=none \
 --opt device=/srv/docker-data/phabricator/data \
 --opt o=bind
docker volume create --name phabricator-repo \
 --opt type=none \
 --opt device=/srv/docker-data/phabricator/repo \
 --opt o=bind

cp extra.cnf /srv/docker-data/phabricator/db-conf

docker-compose up --no-start
 
docker network connect bridge phabricator
docker network connect phabricator ldap