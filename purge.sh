#!/bin/bash

purge() {
	echo "Purging..."
	docker-compose down
	docker volume rm phabricator-db-data phabricator-db-conf phabricator-data phabricator-repo
	sudo rm -rf /srv/docker-data/phabricator
	exit
}

while true
do
	read -r -p "Purge will remove also external volume data. Are you Sure? To only stop container, use stop.sh [yes/no] " input

	case $input in
		[yY][eE][sS])
		purge
		;;

		[nN][oO])
		echo "Bye"
		exit
		;;

		*)
		echo "Pleas enter 'yes' or 'no'"
		;;
	esac
done